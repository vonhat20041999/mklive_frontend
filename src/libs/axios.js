import axios from 'axios';

const AUTH_TOKEN = localStorage.getItem('token')

const VERSION = 'v1'

const instance = axios.create({
  baseURL: 'http://123.25.30.53:6001/api/' + VERSION,
  headers: {'Authorization': AUTH_TOKEN}
})

export default instance