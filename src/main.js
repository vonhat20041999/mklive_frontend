import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.css'
import '@/assets/base.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

library.add(fas, fab, far)

const app = createApp(App)

app
  .component('font-awesome-icon', FontAwesomeIcon)
  .use(router)
  .use(store)
  .use(Toast, {})
  .mount('#app')

import 'bootstrap/dist/js/bootstrap.js'
